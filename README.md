A) COMPUTE SIGMA PROFILES using ADF and Sigma_Average

1. Run a gas phase geometry optimization. 
    Keep t21 file.
→ filename_gp.t21

2. Run a single point COSMO calculation on the gas phase optimized geometry.
    Keep t21 file.
→ filename_cosmors.t21

3. Extract the COSMO information from filename_cosmors.t21 into cosmo.kf file 
    using ADF utilities and transform the cosmo.kf file to a text file cosmo.txt.

Find examples of cosmo.txt output files in  ./examples
                                                                    
4.  Run program  sigma calculation: 

The executable Sigma_average  is in folder  ./bin

Run the program:
./bin/Sigma_average –h
Run options: -i –o –x –n -d
Sigma_average –i input_file –o output_file

Find more output file examples in folder: 

./examples/sigma_Ti2Ta8.out


