PROGRAM SIGMAPROFILEV2
!******************************************************************************************
!CREATED USING DIGITAL VISUAL FORTRAN 6.0 (2006)
!
!******************************************************************************************
!Modified C.Bo @ICIQ 2019-2020 (cbo@iciq.cat)
! This program calculates the sigma-profile from a cosmo.kf file
! as calculated by ADF (convieniently translated to txt format, p.ex., "cosmo.txt").
! The sigma-profile is originally computed in a range from -0.025/0.025, step 0.001.
! This code permits to increase this range and change the step, which
! is usually needed for dealing with highly charged molecules.
!
! - Parameters read from command line
!    -h (prints this help)
!    -i input_filename
!    -o output_filename (default sigma.txt)
!    -x MAX sigma value (default  0.025)
!    -n MIN sigma value (default -0.025)
!    -d Delta value (default 0.001)
!
!******************************************************************************************
! INITIAL Description
!
!	This program reads the modified COSMO output file (in text format) from Accelrys' 
! Materials Studio DMol3 and averages the surface segment charge densities per 
! Klamt (1995), Klamt et al (1998), Klamt et al (2000), Lin and Sandler (2002) to 
! establish the segment charges for the "sigma-profile".  This program creates 
! a text file that MS Excel can read and plot.
!
!	THIS PROGRAM WRITTEN BY:
!		RICHARD OLDLAND (roldland@vt.edu)               MIKE ZWOLAK (zwolak@caltech.edu)
!		DEPARTMENT OF CHEMICAL ENGINEERING              PHYSICS DEPARTMENT
!		VIRGINIA TECH                                   CALIFORNIA INSTITUTE OF TECHNOLOGY
!		BLACKSBURG, VA 24060                            PASADENA, CA 91125
!
!	EDITED MARCH 2006 BY:
!		ERIC MULLINS (pmullins@vt.edu)
!		DEPARTMENT OF CHEMICAL ENGINEERING
!		VIRGINIA TECH
!		BLACKSBURG, VA 24060
!
!	VALUES READ FROM THE DATA FILE:
!	ATOM = ATOM NUMBER IN MOLECULE
!	POSXAU = X-CORDINATE OF THE SEGMENT POSITION IN ATOMIC UNITS
!	POSYAU = Y-CORDINATE OF THE SEGMENT POSITION IN ATOMIC UNITS
!	POSZAU = Z-CORDINATE OF THE SEGMENT POSITION IN ATOMIC UNITS
!	POSXA = X-CORDINATE OF THE SEGMENT POSITION IN ANGSTROMS
!	POSYA = Y-CORDINATE OF THE SEGMENT POSITION IN ANGSTROMS
!	POSZA = Z-CORDINATE OF THE SEGMENT POSITION IN ANGSTROMS
!	A = SURFACE SEGMENT AREA; APPROXIMATED AS CIRCULAR (ANGSTROMS SQUARED)
!	CHG = CHARGE OF THE SURFACE SEGMENT (SIGMA, e)
!	SIGMA = RATIO OF SURFACE CHARGE TO AREA (SIGMA/A, e/A**2)
!	POTENT = SURFACE POTENTIAL
!
!	FROM SEGMENT CHARGE AVERAGING:
!	REFF = RADIUS OF THE AREA THAT AFFECTS THE SURFACE SEGMENT CHARGE (ANGSTROMS)
!       DMN = DISTANCE BETWEEN CALCULATED SEGEMENT AND SEGEMENTS AFFECTING IT (ANGSTROMS)
!	RAD = RADIUS OF THE PARTICULAR SURFACE SEGMENT (ASSUMED CIRCULAR, ANGSTROMS SQUARED)
!	SIGMANEW = NEW AVERAGED SIGMA VALUE
!	SIGMASUM = SUMMATION OF EFFECTS FROM OTHER SURFACE SEGMENTS
!	NORMDIST = NORMALIZATION FACTOR
!	NORMSUM = SUMMATION OF ALL NORMALIZATION FACTORS
!
!	REQUIRED INPUT (ON PROMPT):
!		NAME OF FILE (INCLUDING LOCATION AND EXTENSION)
!		NAME OF CHEMICAL (THIS WILL APPEAR IN THE OUTPUT FILE)
!		NUMSEGMENT = THE NUMBER OF SURFACE SEGMENTS
!
!  THE OUTPUT FILE "'CHEMICAL'SIGMA-PROFILE.TXT" IS THE SORTED SIGMA PROFILE
!
!  LITERATURE CITED:
!  Klamt, A. Conductor-like Screening Model for Real Solvents: A New Approach to the
!       Quantitative Calculation of Solvation Phenomena. J. Phys. Chem 1995, 99, 2224.
!  Klamt, A.; Jonas, V.; Burger, T.; Lohrenz, J. Refinement and Parameterization of 
!	COSMO-RS. J. Phys. Chem A 1998, 102, 5074.
!  Klamt, A.; Eckert, F.; COSMO-RS: A Novel and Efficient Method for the a Priori 
!	Prediction of Thermophysical Data of Liquids.  Fluid Phase Equilibria 2000, 
!	172, 43.
!  Lin, S.T.; Sandler, S. A Priori Phase Equilibrium Prediction from a Segment 
!       Contribution Solvation Model. Ind. Eng. Chem. Res, 2002, 41, 899 
!  Lin, S.T.;  Quantum Mechanical Approaches to the Prediction of Phase Equilibria: 
!	Solvation Thermodynamics and Group Contribution Methods, PhD. Dissertation, 
!	University of Delaware, Newark, DE, 2000
!******************************************************************************************
use f90getopt
IMPLICIT NONE

CHARACTER(128):: FILEINDEX, FILEOUTPUT
CHARACTER(16):: CHEMICAL
CHARACTER(1) :: RESPONSE
CHARACTER(5) :: CHAR5
CHARACTER (256) :: FILENAME
INTEGER :: I, J, K, F, M, N, O, DUMBI, TMP, NUMSEGMENT, NATOMS
INTEGER, DIMENSION (:), ALLOCATABLE :: ATOM
!INTEGER, DIMENSION (1500) :: FILEIN, FILEOUT
INTEGER FILEIN, FILEOUT
REAL*8 :: REFF, PI, FDEC, SUM
REAL*8, DIMENSION (:), ALLOCATABLE :: POSXAU, POSYAU, POSZAU, POSXA, POSYA, POSZA, A
REAL*8, DIMENSION (:), ALLOCATABLE :: CHG, SIGMA, POTENT, SIGMANEW, SIGMASUM, RAD, NORMDIST
REAL*8, DIMENSION (:), ALLOCATABLE :: NORMSUM, DMN
REAL*8, DIMENSION(:), ALLOCATABLE :: CHGDEN,SP
REAL*8 :: MAX,MIN,DELTA
INTEGER :: NPOINTS
logical :: res

!******************************************************************************************
!character:: ch ! Unused: ch=getopt()
! START For longopts only
type(option_s):: opts(5)
!opts(1) = option_s( "input", .false., 'i' )
!opts(2) = option_s( "output",  .true.,  'o' )
!opts(3) = option_s( "help", .false.,  'h')
! END longopts
!
!ESTABLISH CONSTANTS
    PI = 3.14159265358979
!******************************************************************************************
!
! Modif. C.Bo
! in the orginal version REFF = 1.43633626471067
! REFF=0.5 as in J. Phys. Chem. A 1998, 102, 5074-5085
! This value reproduces the sigma profile as implemented in ADF
! following Can. J. Chem. 87: 790–797 (2009)
!
!REFF = 1.43633626471067
!******************************************************************************************
REFF = 0.5D0
FDEC = 1.00000000000000
!
!******************************************************************************************
!
! default values
!
! MAX/MIN values of the sigma-profile.
!
MAX=0.05D0
MIN=-0.05D0
DELTA=0.001D0
FILENAME=''
FILEOUTPUT = 'sigma.txt'
!
! If no options were committed
if (command_argument_count() .eq. 0 ) then
  write (*,*) "Available Options: -h -i -o -x -n -d"
  Stop
end if
!
! Process options one by one
    do
        select case( getopt( "i:o:x:n:d:h", opts ) ) ! opts is optional (for longopts only)
            case( char(0) )
                exit
            case( 'i' )
                inquire(file=optarg,exist=res)
                if (res) then
                    FILENAME = optarg
                    print *, 'Non-default input file: ', optarg
                 else
                    print *, 'Sorry. Requested input file does not exist. Stop'
                    Stop
                endif
            case( 'o' )
                print *, 'Redirecting output to file: ', optarg
                FILEOUTPUT = optarg
            case( 'x' )
                print *, 'MAX non-default value: ', optarg
                read(optarg,*) MAX
            case( 'n' )
                print *, 'MIN non-default value: ', optarg
                read(optarg,*) MIN
            case( 'd' )
                print *, 'Delta non-default value: ', optarg
                read(optarg,*) DELTA
            case( 'h' )
                print *
print *,' This program calculates the sigma-profile from a cosmo.kf file'
print *,' as calculated by ADF (convieniently translated to txt format, p.ex., "cosmo.txt").'
print *,' The sigma-profile is originally computed in a range from -0.025/0.025, step 0.001.'
print *,' This code permits to increase this range and change the step, which'
print *,' is usually needed for dealing with highly charged molecules.'
print *,
print *,' - Parameters read from command line'
print *,'    -h (prints this help)'
print *,'    -i input_filename'
print *,'    -o output_filename (default sigma.txt)'
print *,'    -x MAX sigma value (default  0.025)'
print *,'    -n MIN sigma value (default -0.025)'
print *,'    -d Delta value (default 0.001)'
print *

                Stop
        end select
    end do

FILEIN=11
FILEOUT=12
inquire(file=FILENAME,exist=res)
if (res) then
    OPEN (UNIT=FILEIN, FILE = FILENAME, STATUS="unknown")
 else
    print *, 'Sorry. Requested input file does not exist. Stop'
    stop
endif

OPEN (UNIT=FILEOUT, FILE = FILEOUTPUT,STATUS="unknown")


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! old
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ESTABLISH OUTPUT FILE UNIT NUMBERS
!F=1
!DO O = 1, 1500
!   FILEOUT(O) = O+1521
!END DO
!REPETITION LOOP TO CALCULATE MULTIPLE P(S)
!DO F = 1, 1500

   ! INPUT VARIABLES DEFINED FROM ARRAYS FILENAME AND NUMBSEGMENT
   !FILELOOP = 'C:\COSMO\VT-2006_AMBER\'//FILENAME(F)
   !FILEOUTPUT = 'C:\PROFILES\VT-2006_AMBER\'//FILENAME(F)
   !NUMSEGMENT = NUMBSEGMENT(F)

   !ESTABLISHING THE COSMO FILE TO READ
!   WRITE(*,*) "TYPE THE NAME OF THE FILE YOU WISH TO READ IN,"
!   WRITE(*,*) "INCLUDING LOCATION (MAX 256 CHARACTERS), AND HIT ENTER"
!   READ (*,*) FILENAME

   !ESTABLISH THE CHEMICAL NAME
!   WRITE(*,*) "TYPE IN THE NAME OF THE CHEMICAL (MAX 16 CHARACTERS), AND HIT ENTER"
!   WRITE(*,*) "SIGMA PROFILE OUTPUT FILE WILL BE CREATED IN C:\PROFILES\"
!   WRITE(*,*) "IF THIS DIRECTORY DOES NOT EXIST, PLEASE CREATE IT AT THIS TIME."
!   READ (*,*) CHEMICAL

! FILEOUTPUT = 'C:\PROFILES\'//CHEMICAL//'.TXT'
   !OPEN THE COSMO FILE WITH ALL SEGMENTS AND CORRESPONDING CHARGE DENSITIES
!   OPEN(UNIT=FILEIN(F), FILE = FILENAME, STATUS = "OLD", ACTION = "READ", POSITION = "REWIND")

   !ESTABLISH THE NUMBER OF SURFACE SEGMENTS AND ALLOCATE THE ARRAYS
!   WRITE(*,*) "TYPE THE NUMBER OF SURFACE SEGMENTS, FROM THE COSMO OUTPUT, AND HIT ENTER"
!   READ (*,*) NUMSEGMENT
!   modified to read directly from input file
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!******************************************************************************************
!
! READ THE COSMO FILE AS GENERATED BY ADF and translated to txt
!
! READ FIRST LINE. it SHOULD READS "$coor"
    READ(FILEIN,*,end=198) CHAR5
        if (char5.ne.'$coor') then
            print *, 'Incorrect format in input file'
            stop
        endif
      goto 199
 198  stop 'File empty'
 199 continue
! count atoms
    NATOMS=0
    DO WHILE (1.EQ.1)
        READ(FILEIN,*,end=200) CHAR5
        if (CHAR5.NE.'$cosm') then
           NATOMS=NATOMS+1
        else
            go to 200
        endif
    enddo
200 continue
   print *, 'Number of atoms = ', NATOMS
! finish read atoms, now read 14 lines till begining segments
   do i=1, 14
   READ(FILEIN,*)
   enddo
   NUMSEGMENT=0
!READ THE numer of segments AND ESTABLISH THE SIZE OF THE DATA ARRAYS
    I=0
    DO WHILE (1.eq.1)
      READ(FILEIN,*, err=300) CHAR5
     if (char5.ne.'$sigm') then
            i=i+1
        else
            goto 300
      endif
    enddo
300 continue
    NUMSEGMENT=i
    print *, 'Number of segments = ', NUMSEGMENT
    ALLOCATE(ATOM(NUMSEGMENT), POSXAU(NUMSEGMENT), POSYAU(NUMSEGMENT), &
     POSZAU(NUMSEGMENT), POSXA(NUMSEGMENT), POSYA(NUMSEGMENT), &
     POSZA(NUMSEGMENT), A(NUMSEGMENT), CHG(NUMSEGMENT), SIGMA(NUMSEGMENT), &
     POTENT(NUMSEGMENT), SIGMANEW(NUMSEGMENT), SIGMASUM(NUMSEGMENT), &
     RAD(NUMSEGMENT), NORMDIST(NUMSEGMENT), NORMSUM(NUMSEGMENT), DMN(NUMSEGMENT))

! rewind the file, and now read again
      rewind(FILEIN)
      do i=1, 1+NATOMS+1+14
         READ(FILEIN,*)
      enddo
!******************************************************************************************
      SUM=0.
      DO i=1, NUMSEGMENT
       READ(FILEIN,*, err=300) CHAR5,ATOM(I),POSXAU(I),POSYAU(I),POSZAU(I),CHG(I),A(I),SIGMA(I),POTENT(I)
!CONVERT THE POSITIONS FROM ATOMIC UNITS TO ANGSTROMS AND ASSIGN NEW ARRAYS
	  POSXA(I) = POSXAU(I) * 0.529177249 
	  POSYA(I) = POSYAU(I) * 0.529177249
	  POSZA(I) = POSZAU(I) * 0.529177249
	  RAD(I) = SQRT(A(I)/PI)
      SUM=SUM+CHG(I)
     END DO

    print *, 'Sum of charges in segments = ', SUM

!CLOSE COSMO FILE
   CLOSE(FILEIN)

!BEGIN AVERAGING SURFACE CHARGES
   DO J=1, NUMSEGMENT
	  SIGMANEW(J) = 0.D0
	  NORMSUM(J)=0.D0
      sum=0.
	  
	  DO K=1, NUMSEGMENT
		 DMN(K) = SQRT((POSXA(K)-POSXA(J))**2+(POSYA(K)-POSYA(J))**2+ &
			     (POSZA(K)-POSZA(J))**2)
		 SIGMASUM(K)= SIGMA(K)*(RAD(K)**2*REFF**2)/(RAD(K)**2+REFF**2)* &
			          DEXP(-FDEC*(DMN(K)**2)/(RAD(K)**2+REFF**2))
		 NORMDIST(K) =(RAD(K)**2*REFF**2)/(RAD(K)**2+REFF**2)* &
			          DEXP(-FDEC*(DMN(K)**2)/(RAD(K)**2+REFF**2))
		 NORMSUM(J) = NORMSUM(J) + NORMDIST(K)
		 SIGMANEW(J) = SIGMANEW(J) + SIGMASUM(K)
      END DO

	SIGMANEW(J) = SIGMANEW(J)/NORMSUM(J)

   END DO

!allocate arrays to hold the histogram in CHGDEN values
    NPOINTS=INT((MAX-MIN)/DELTA)+1
    ALLOCATE(SP(NPOINTS+2),CHGDEN(NPOINTS+2))
    DO J=1,NPOINTS
	  SP(J)=0.D0
	  CHGDEN(J) = MIN+DELTA*DBLE(J-1)
    END DO

!SIGMA PROFILE SORTING TAKEN FROM LIN DISSERTATION**
   DO J=1,NUMSEGMENT
	  TMP=INT((SIGMANEW(J)-CHGDEN(1))/DELTA)
      if (TMP.GE.NPOINTS) then
        print *, "*"
        print *, "*** Warning: ", J, " segment with value", SIGMANEW(J)," out of range"
        print *, "*"
        print *, "* Current NPOINTS ", NPOINTS
        print *, "* Index required: ", TMP+1
        print *, "* Current MAX & MIN values ",MAX, MIN
        print *, "*"
        print *, "* You need to increase them !!! Sorry. Stop"
        Stop
      endif

	  SP(TMP+1)=SP(TMP+1)+A(J)*(CHGDEN(TMP+2)-SIGMANEW(J))/DELTA
	  SP(TMP+2)=SP(TMP+2)+A(J)*(SIGMANEW(J)-CHGDEN(TMP+1))/DELTA

   END DO

! WRITE SP, WHICH CONTAINS AVERAGED SIGMA-PROFILE
    OPEN (FILEOUT, FILE = FILEOUTPUT)
    DO J=1,NPOINTS
	  WRITE(FILEOUT,100) CHGDEN(J),SP(J)
    END DO
100 format(F8.4,',',F8.4)
    CLOSE(FILEOUT)

        WRITE(6,111) ' Sigma profile from ', MIN,' to', MAX,' with',NPOINTS, ' points generated in file ',FILEOUTPUT
111     format(A,F5.2,A,F5.2,A,I6,A,A)


   !REPEAT SIGMA PROFILE CALCULATION FOR ANOTHER COMPOUND
!   WRITE (*,*) "DO YOU WISH TO CALCULATE ANOTHER SIGMA PROFILE (Y or N)"
!   READ (*,*) RESPONSE

!   IF (RESPONSE=="N") EXIT


!END DO
 

END PROGRAM SIGMAPROFILEV2
